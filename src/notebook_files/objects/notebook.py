from typing import Dict, List, Tuple
from notebook_files.objects.user_story import UserStory
from notebook_files.objects.file_manager import FileManager
from notebook_files.utils.constans import NOTEBOOK_FIELDS
from notebook_files.utils.helpers import is_field_valid


class Notebook:

    def __init__(self):
        self.user_story = UserStory()
        self.file_manager = FileManager()

    def __create_new_record(self) -> Dict:
        local_record = {field: '' for field in NOTEBOOK_FIELDS.keys()}
        for field, required in NOTEBOOK_FIELDS.items():
            is_valid = False
            while not is_valid:
                is_required = '*' if required == 'required' else ''
                row = self.user_story.ask_user(f'Please enter your "{field.title()}"{is_required}: ')

                is_valid, message = is_field_valid(field, row)

                if not is_valid:
                    self.user_story.send_user(f'[ERROR] {message}')
                else:
                    local_record[field] = row

        return local_record

    def __create_notebook_order_list(self) -> List:
        file_data = self.file_manager.read_files()

        if len(file_data) > 0:
            output_data = []
            for index in range(len(file_data)):
                elem = {'id': index}
                elem.update(file_data[index])
                output_data.append(elem)

            headers = ['index']
            headers.extend(list(NOTEBOOK_FIELDS.keys()))
            self.user_story.show_output(output_data, headers=headers)

            return file_data
        else:
            self.user_story.send_user('Sorry, there are no rows')
            return []

    def __get_phone_data(self) -> Tuple[List, List]:
        find_data = []
        output_data = []
        phone_num = ''

        is_valid = False
        while not is_valid:
            phone_num = self.user_story.ask_user('Please enter phone number: ')
            is_valid, message = is_field_valid('phone_number', phone_num)

            if not is_valid:
                self.user_story.send_user(message)

        find_data1 = self.file_manager.read_files()
        for row in find_data1:
            if row['phone_number'] == phone_num:
                find_data.append(row)
            else:
                output_data.append(row)
        return find_data, output_data

    def __sort_by_param(self, param: str) -> List:
        file_data = self.file_manager.read_files()
        output_data = sorted(file_data, key=lambda r: r[param])
        return output_data

    def show_notebook(self):
        file_data = self.file_manager.read_files()
        self.user_story.show_output(file_data)

    def add_record(self):
        local_record = self.__create_new_record()

        self.file_manager.write_file(local_record)
        self.user_story.send_user('-' * 50)
        self.user_story.send_user('New record was successfully added')

    def remove_record(self):
        file_data = self.__create_notebook_order_list()
        if not file_data:
            return

        while True:
            try:
                choice = int(self.user_story.ask_user('Please choose index to remove: '))

                file_data.pop(choice)

                self.file_manager.create_new_file(file_data)
                self.user_story.send_user('-' * 50)
                self.user_story.send_user('Record was successfully removed')

                break
            except ValueError:
                self.user_story.send_user('ERROR. Please enter correct value')
            except IndexError:
                self.user_story.send_user('ERROR. Please enter correct index')

    def edit_record(self):
        file_data = self.__create_notebook_order_list()
        if not file_data:
            return
        while True:
            try:
                choice = int(self.user_story.ask_user('Please choose index to edit: '))

                file_data.pop(choice)

                local_record = self.__create_new_record()

                file_data.append(local_record)

                self.file_manager.create_new_file(file_data)
                self.user_story.send_user('-' * 50)
                self.user_story.send_user('Record was successfully edited')

                break
            except ValueError:
                self.user_story.send_user('ERROR. Please enter correct value')
            except IndexError:
                self.user_story.send_user('ERROR. Please enter correct index')

    def search_by_name(self):
        output_data = []
        name = ''
        is_valid = False
        while not is_valid:
            name = self.user_story.ask_user('Please enter record name: ').lower()
            is_valid, message = is_field_valid('name', name)

            if not is_valid:
                self.user_story.send_user(message)

        file_data = self.file_manager.read_files()
        for row in file_data:
            if row['name'].lower() == name:
                output_data.append(row)
        self.user_story.show_output(output_data)

    def search_by_telephone_number(self):
        output_data, other_data = self.__get_phone_data()
        self.user_story.show_output(output_data)

    def sort_by_name(self):
        output_data = self.__sort_by_param('name')
        self.user_story.show_output(output_data)

    def sort_by_surname(self):
        output_data = self.__sort_by_param('surname')
        self.user_story.show_output(output_data)

    def turn_off(self):
        self.user_story.send_user('Good bye!')
        exit(0)


