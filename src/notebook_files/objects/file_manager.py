import os
import csv
from typing import List, Dict
from src.notebook_files.utils.constans import NOTEBOOK_FIELDS


class FileManager:
    def __init__(self, file_name: str = 'notebook.csv'):
        self.file_path = '/'.join([os.getcwd(), 'models', file_name])

        if not os.path.exists(self.file_path):
            self.create_new_file()

    def read_files(self) -> List:
        rows = []
        with open(self.file_path, mode='r') as csv_file:
            csv_reader = csv.DictReader(csv_file)
            for row in csv_reader:
                rows.append(row)
        return rows

    def create_new_file(self, rows: List = None):
        with open(self.file_path, mode='w') as csv_file:
            fields_names = list(NOTEBOOK_FIELDS.keys())
            writer = csv.DictWriter(csv_file, fieldnames=fields_names)
            writer.writeheader()

            if rows is not None:
                for row in rows:
                    writer.writerow(row)

    def write_file(self, row: Dict):
        with open(self.file_path, mode='a') as csv_file:
            fields_names = list(NOTEBOOK_FIELDS.keys())
            writer = csv.DictWriter(csv_file, fieldnames=fields_names)
            writer.writerow(row)


