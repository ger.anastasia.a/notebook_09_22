from notebook_files.objects.file_manager import FileManager
from notebook_files.utils.constans import MENU
from typing import List
from tabulate import tabulate
from notebook_files.utils.constans import NOTEBOOK_FIELDS


class UserInterface:

    @staticmethod
    def send_user(message: str):
        print(message)

    @staticmethod
    def ask_user(message: str):
        user_data = input(message).strip()
        return user_data

    def show_output(self, output_data: List, headers: List = None):
        headers = list(NOTEBOOK_FIELDS.keys())

        if len(output_data) == 0:
            self.send_user('There are no records to show')
        else:
            self.send_user('-' * 50)

        tabulate_list = [list(row.values()) for row in output_data]
        self.send_user(tabulate(tabulate_list, headers=headers))


class UserStory(UserInterface):
    def __init__(self):
        self.file_manager = FileManager()

    def show_notebook(self):
        self.send_user('-' * 50)
        file_data = self.file_manager.read_files()
        if len(file_data) == 0:
            self.send_user('Your NoteBook is empty')
        else:
            self.send_user(f'You have {len(file_data)} record(-) in NoteBook')

    def show_menu(self):
        self.send_user('-' * 50)
        for user_choice, data in MENU.items():
            self.send_user(f'{user_choice}. {data["name"]}')
        self.send_user('-' * 50)

    def run_command(self, command: int, notebook):
        getattr(notebook(), MENU[command]['function'])()

        self.send_user('-' * 50)
        self.ask_user('Enter any key...')

    @staticmethod
    def get_correct_menu_items() -> List:
        return list(MENU.keys())



