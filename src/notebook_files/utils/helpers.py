from typing import Tuple
import re
import datetime


def is_field_valid(field: str, data: str) -> Tuple[bool, str]:
    match field:
        case 'name':
            return (
                data != '' and len(data) > 2,
                'Please enter field. Length might be more than 2 symbols'
            )
        case 'surname':
            return (
                data != '' and len(data) > 2,
                'Please enter field. Length might be more than 2 symbols'
            )
        case 'phone_number':
            return (
                data != '' and re.match('^\\d{10}$', data) is not None,
                'Please enter valid phone number'
            )
        case 'birthday':
            try:
                datetime.datetime.strptime(data, '%Y-%m-%d'),
                return True, ''
            except ValueError:
                return False, 'Please enter valid date YYYY-MM-DD'
        case _:
            return True, ''
