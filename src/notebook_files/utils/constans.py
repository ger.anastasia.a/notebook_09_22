
NOTEBOOK_FIELDS = {
    'name': 'required',
    'surname': 'required',
    'phone_number': 'required',
    'address': '',
    'birthday': ''
}


MENU = {
    0: {
        'name': 'Show notebook',
        'function': 'show_notebook'
    },

    1: {
        'name': 'Add record',
        'function': 'add_record'
    },
    2: {
        'name': 'Remove record',
        'function': 'remove_record'
    },
    3: {
        'name': 'Change record',
        'function': 'edit_record'
    },
    4: {
        'name': 'Search by name',
        'function': 'search_by_name'
    },
    5: {
        'name': 'Search by telephone number',
        'function': 'search_by_telephone_number'
    },
    6: {
        'name': 'Sort by name',
        'function': 'sort_by_name'
    },
    7: {
        'name': 'Sort by surname',
        'function': 'sort_by_surname'
    },
    8: {
        'name': 'Exit',
        'function': 'turn_off'
    }
}
