from notebook_files.objects.user_story import UserStory
from notebook_files.objects.notebook import Notebook

if __name__ == '__main__':
    user_story = UserStory()
    user_story.show_notebook()
    while True:
        user_story.show_menu()

        try:
            choice = int(user_story.ask_user('Please make your choice: '))
        except ValueError:
            user_story.send_user('Please enter only correct data from menu')
            continue

        if choice not in user_story.get_correct_menu_items():
            user_story.send_user('Wrong choice. Please try again')
            continue

        user_story.run_command(choice, Notebook)

